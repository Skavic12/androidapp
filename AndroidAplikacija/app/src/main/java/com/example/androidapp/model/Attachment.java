package com.example.androidapp.model;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Attachment implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("dataBase")
    private Base64 dataBase;
    @SerializedName("tip")
    private String tip;
    @SerializedName("ime")
    private String ime;
    @SerializedName("porukaAtt")
    private EmailClass porukaAtt;

    public Attachment(){
        super();
    }

    public Attachment(int id, Base64 dataBase, String tip, String ime, EmailClass porukaAtt) {
        this.id = id;
        this.dataBase = dataBase;
        this.tip = tip;
        this.ime = ime;
        this.porukaAtt = porukaAtt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Base64 getDataBase() {
        return dataBase;
    }

    public void setDataBase(Base64 dataBase) {
        this.dataBase = dataBase;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public EmailClass getPorukaAtt() {
        return porukaAtt;
    }

    public void setPorukaAtt(EmailClass porukaAtt) {
        this.porukaAtt = porukaAtt;
    }
}
