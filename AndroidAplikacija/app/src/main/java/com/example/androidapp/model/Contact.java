package com.example.androidapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Contact  implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("ime")
    private String ime;
    @SerializedName("prezime")
    private String prezime;
    @SerializedName("email")
    private String email;
    @SerializedName("display")
    private Photo display;
    private enum format {PLAIN, HTML};
    @SerializedName("from")
    private ArrayList<EmailClass> from;
    @SerializedName("to")
    private ArrayList<EmailClass> to;
    @SerializedName("cc")
    private ArrayList<EmailClass> cc;
    @SerializedName("bcc")
    private ArrayList<EmailClass> bcc;



    public Contact() {
        super();
    }

    public Contact(int id, String ime, String prezime, String email, Photo display, ArrayList<EmailClass> from, ArrayList<EmailClass> to, ArrayList<EmailClass> cc, ArrayList<EmailClass> bcc) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.display = display;
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Photo getDisplay() {
        return display;
    }

    public void setDisplay(Photo display) {
        this.display = display;
    }

    public ArrayList<EmailClass> getFrom() {
        return from;
    }

    public void setFrom(ArrayList<EmailClass> from) {
        this.from = from;
    }

    public ArrayList<EmailClass> getTo() {
        return to;
    }

    public void setTo(ArrayList<EmailClass> to) {
        this.to = to;
    }

    public ArrayList<EmailClass> getCc() {
        return cc;
    }

    public void setCc(ArrayList<EmailClass> cc) {
        this.cc = cc;
    }

    public ArrayList<EmailClass> getBcc() {
        return bcc;
    }

    public void setBcc(ArrayList<EmailClass> bcc) {
        this.bcc = bcc;
    }

    @Override
    public String toString() {
        return  email ;
    }
}
