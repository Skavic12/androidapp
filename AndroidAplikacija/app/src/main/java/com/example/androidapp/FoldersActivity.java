package com.example.androidapp;

import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.androidapp.Adapter.EmailListAdapter;
import com.example.androidapp.Adapter.FolderListAdapter;
import com.example.androidapp.Retrofit.FoldersI;
import com.example.androidapp.Retrofit.MessagesI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.EmailClass;
import com.example.androidapp.model.Folder;
import com.example.androidapp.model.Objekti;
import com.example.androidapp.model.Rule;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoldersActivity extends AppCompatActivity {

    private FloatingActionButton actionButton;
    private DrawerLayout meniFolders;
    private ActionBarDrawerToggle nFtoggle;
    public static ArrayList<Folder> sviFolderi = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folders);

        Toolbar Fstoolbar = findViewById(R.id.foldersToolbar);
        setSupportActionBar(Fstoolbar);

        getSupportActionBar().setTitle("Folders");

        actionButton = findViewById(R.id.foldersFAB);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(FoldersActivity.this , CreateFolderActivity.class);
                startActivity(i);
                return;
            }
        });

        meniFolders = (DrawerLayout) findViewById(R.id.folders_drawer);
        nFtoggle = new ActionBarDrawerToggle(this,meniFolders,R.string.open,R.string.close);
        meniFolders.addDrawerListener(nFtoggle);
        nFtoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navView = findViewById(R.id.nav_view_folders);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.emails_menu:
                        Intent em = new Intent(FoldersActivity.this,EmailsActivity.class);
                        startActivity(em);
                        break;
                    case R.id.folders_menu:
                        Intent fm = new Intent(FoldersActivity.this,FoldersActivity.class);
                        startActivity(fm);
                        break;
                    case R.id.contacts_menu:
                        Intent cm = new Intent(FoldersActivity.this,ContactsActivity.class);
                        startActivity(cm);
                        break;
                    case R.id.drafts_nav:
                        Intent c = new Intent(FoldersActivity.this,DraftActivity.class);
                        startActivity(c);
                        break;
                    case R.id.setting_menu:
                        Intent sm = new Intent(FoldersActivity.this,SettingsActivity.class);
                        startActivity(sm);
                        break;
                    case R.id.logout_nav:
                        SaveSharedPreference.getLoggedStatus(getApplicationContext());
                        break;
                }
                return  true;
            }

        });

        View headW = navView.getHeaderView(0);
        ImageView profil = headW.findViewById(R.id.proFilsHEd);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FoldersActivity.this , ProfileActivity.class);
                startActivity(i);
                return;
            }
        });

        FoldersI me = RetrofitC.getClient().create(FoldersI.class);
        Call<ArrayList<Folder>> call = me.getFolders();

        call.enqueue(new Callback<ArrayList<Folder>>() {
            @Override
            public void onResponse(Call<ArrayList<Folder>> call, Response<ArrayList<Folder>> response) {
                sviFolderi = response.body();

                ListView mList = findViewById(R.id.listaFoldera);
                FolderListAdapter fla = new FolderListAdapter(FoldersActivity.this,R.layout.layout_list_folders,sviFolderi);
                mList.setAdapter(fla);

                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                    {
                        Intent folder = new Intent(FoldersActivity.this, FolderActivity.class);
                        folder.putExtra("FOLDER", sviFolderi.get(position));

                        startActivity(folder);

                    }
                });

            }

            @Override
            public void onFailure(Call<ArrayList<Folder>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if(nFtoggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
