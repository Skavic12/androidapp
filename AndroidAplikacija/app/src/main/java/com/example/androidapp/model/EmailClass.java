package com.example.androidapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class EmailClass implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("from")
    private Contact from;
    @SerializedName("to")
    private Contact to;
    @SerializedName("cc")
    private ArrayList<Contact> cc;
    @SerializedName("bcc")
    private ArrayList<Contact> bcc;
    @SerializedName("vreme")
    private Date vreme;
    @SerializedName("tema")
    private String tema;
    @SerializedName("sadrzajPor")
    private String sadrzajPor;
    @SerializedName("folderP")
    private Folder folderP;
    @SerializedName("tagovi")
    private ArrayList<Tag> tagovi;
    @SerializedName("prilog")
    private ArrayList<Attachment> prilog;
    @SerializedName("seen")
    private boolean seen;


    public EmailClass() {
        super();
    }

    public EmailClass(int id, Contact from, Contact to, ArrayList<Contact> cc, ArrayList<Contact> bcc, Date vreme, String tema, String sadrzajPor, Folder folderP, ArrayList<Tag> tagovi, ArrayList<Attachment> prilog, boolean seen) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.vreme = vreme;
        this.tema = tema;
        this.sadrzajPor = sadrzajPor;
        this.folderP = folderP;
        this.tagovi = tagovi;
        this.prilog = prilog;
        this.seen = seen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contact getFrom() {
        return from;
    }

    public void setFrom(Contact from) {
        this.from = from;
    }

    public Contact getTo() {
        return to;
    }

    public void setTo(Contact to) {
        this.to = to;
    }

    public ArrayList<Contact> getCc() {
        return cc;
    }

    public void setCc(ArrayList<Contact> cc) {
        this.cc = cc;
    }

    public ArrayList<Contact> getBcc() {
        return bcc;
    }


    public void setBcc(ArrayList<Contact> bcc) {
        this.bcc = bcc;
    }

    public Date getVreme() {
        return vreme;
    }

    public void setVreme(Date vreme) {
        this.vreme = vreme;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getSadrzajPor() {
        return sadrzajPor;
    }

    public void setSadrzajPor(String sadrzajPor) {
        this.sadrzajPor = sadrzajPor;
    }

    public Folder getFolderP() {
        return folderP;
    }

    public void setFolderP(Folder folderP) {
        this.folderP = folderP;
    }

    public ArrayList<Tag> getTagovi() {
        return tagovi;
    }

    public void setTagovi(ArrayList<Tag> tagovi) {
        this.tagovi = tagovi;
    }

    public ArrayList<Attachment> getPrilog() {
        return prilog;
    }

    public void setPrilog(ArrayList<Attachment> prilog) {
        this.prilog = prilog;
    }

    public boolean isSeen() { return seen; }

    public void setSeen(boolean seen) { this.seen = seen; }
}
