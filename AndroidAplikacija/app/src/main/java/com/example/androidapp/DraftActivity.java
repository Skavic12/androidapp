package com.example.androidapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.androidapp.Adapter.DraftListAdapter;
import com.example.androidapp.Retrofit.DraftI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.EmailClass;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DraftActivity extends AppCompatActivity {


    private DrawerLayout meniDraft;
    private ActionBarDrawerToggle nFtoggle;
    public static ArrayList<EmailClass> sviDrafts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft);

        Toolbar Fstoolbar = findViewById(R.id.DraftToolbar);
        setSupportActionBar(Fstoolbar);

        getSupportActionBar().setTitle("Drafts");

        meniDraft = (DrawerLayout) findViewById(R.id.draft_drawer);
        nFtoggle = new ActionBarDrawerToggle(this,meniDraft,R.string.open,R.string.close);
        meniDraft.addDrawerListener(nFtoggle);
        nFtoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navView = findViewById(R.id.nav_view_folders);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.emails_menu:
                        Intent em = new Intent(DraftActivity.this,EmailsActivity.class);
                        startActivity(em);
                        break;
                    case R.id.folders_menu:
                        Intent fm = new Intent(DraftActivity.this,FoldersActivity.class);
                        startActivity(fm);
                        break;
                    case R.id.contacts_menu:
                        Intent cm = new Intent(DraftActivity.this,ContactsActivity.class);
                        startActivity(cm);
                        break;
                    case R.id.drafts_nav:
                        Intent c = new Intent(DraftActivity.this,DraftActivity.class);
                        startActivity(c);
                        break;
                    case R.id.setting_menu:
                        Intent sm = new Intent(DraftActivity.this,SettingsActivity.class);
                        startActivity(sm);
                        break;
                    case R.id.logout_nav:
                        SaveSharedPreference.getLoggedStatus(getApplicationContext());
                        break;
                }
                return  true;
            }

        });

        View headW = navView.getHeaderView(0);
        ImageView profil = headW.findViewById(R.id.proFilsHEd);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DraftActivity.this , ProfileActivity.class);
                startActivity(i);
                return;
            }
        });

        DraftI me = RetrofitC.getClient().create(DraftI.class);
        Call<ArrayList<EmailClass>> call = me.getDrafts();

        call.enqueue(new Callback<ArrayList<EmailClass>>() {
            @Override
            public void onResponse(Call<ArrayList<EmailClass>> call, Response<ArrayList<EmailClass>> response) {
                sviDrafts = response.body();

                ListView mList = findViewById(R.id.listaDraft);
                DraftListAdapter fla = new DraftListAdapter(DraftActivity.this,R.layout.layout_email_list,sviDrafts);
                mList.setAdapter(fla);

                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                    {
                        Intent folder = new Intent(DraftActivity.this, DraftCRActivity.class);
                        folder.putExtra("DRAFT", sviDrafts.get(position));

                        startActivity(folder);

                    }
                });

            }

            @Override
            public void onFailure(Call<ArrayList<EmailClass>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if(nFtoggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
