package com.example.androidapp.Retrofit;

import com.example.androidapp.model.Contact;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactI {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET("contacts")
    Call<ArrayList<Contact>> getContacts();

    @POST("contacts/{ime}/{prezime}/{email}")
    Call<Contact> dodavanjeKontakt(@Path("ime") String first, @Path("prezime") String last, @Path("email") String email);

    @PUT("contacts/{id}/{ime}/{prezime}/{email}")
    Call<Contact> izmenaKontakta(@Path("id") int id,@Path("ime") String ime,@Path("prezime") String prezime,@Path("email") String email);

    @DELETE("contacts/{id}")
    Call<Void> brsianjeKontakta(@Path("id") int id);

}
