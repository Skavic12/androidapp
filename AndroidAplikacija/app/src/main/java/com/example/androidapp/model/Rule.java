package com.example.androidapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Rule implements Serializable {

    @SerializedName("id")
    private int id;

    public enum Condition {TO, FROM, CC, SUBJECT}

    ;

    public enum Operation {MOVE, COPY, DELETE}

    ;

    @SerializedName("condition")
    private Condition condition;
    @SerializedName("operation")
    private Operation operation;

    public Rule() {
        super();
    }


    public Rule(int id, Condition condition, Operation operation) {
        super();
        this.id = id;
        this.condition = condition;
        this.operation = operation;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Condition getCondition() {
        return condition;
    }


    public void setCondition(Condition condition) {
        this.condition = condition;
    }


    public Operation getOperation() {
        return operation;
    }


    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
