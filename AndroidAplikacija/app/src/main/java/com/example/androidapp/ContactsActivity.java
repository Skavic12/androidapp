package com.example.androidapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.androidapp.Adapter.ContactListAdapter;
import com.example.androidapp.Adapter.EmailListAdapter;
import com.example.androidapp.Retrofit.ContactI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.Account;
import com.example.androidapp.model.Contact;
import com.example.androidapp.model.EmailClass;
import com.example.androidapp.model.Objekti;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsActivity extends AppCompatActivity {

    private DrawerLayout meniC;
    private ActionBarDrawerToggle nCtoggle;
    private FloatingActionButton actionButton;
    private ArrayList<Contact> sviKontakti;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);



        Toolbar toolbar = findViewById(R.id.contactsToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Contacts");

        actionButton = findViewById(R.id.contactsFAB);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactsActivity.this , CreateContactActivity.class);
                startActivity(i);
                return;
            }
        });



        meniC= (DrawerLayout) findViewById(R.id.contacts_drawer);
        nCtoggle = new ActionBarDrawerToggle(this,meniC,R.string.open,R.string.close);
        meniC.addDrawerListener(nCtoggle);
        nCtoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navView = findViewById(R.id.nav_view_contacts);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.emails_menu:
                        Intent em = new Intent(ContactsActivity.this,EmailsActivity.class);
                        startActivity(em);
                        break;
                    case R.id.folders_menu:
                        Intent fm = new Intent(ContactsActivity.this,FoldersActivity.class);
                        startActivity(fm);
                        break;
                    case R.id.contacts_menu:
                        Intent cm = new Intent(ContactsActivity.this,ContactsActivity.class);
                        startActivity(cm);
                        break;
                    case R.id.drafts_nav:
                        Intent c = new Intent(ContactsActivity.this,DraftActivity.class);
                        startActivity(c);
                        break;
                    case R.id.setting_menu:
                        Intent sm = new Intent(ContactsActivity.this,SettingsActivity.class);
                        startActivity(sm);
                        break;
                    case R.id.logout_nav:
                        SaveSharedPreference.getLoggedStatus(getApplicationContext());
                        break;
                }
                return  true;
            }

        });

        View headW = navView.getHeaderView(0);
        ImageView profil = headW.findViewById(R.id.proFilsHEd);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactsActivity.this , ProfileActivity.class);
                startActivity(i);
                return;
            }
        });

        ContactI me = RetrofitC.getClient().create(ContactI.class);
        Call<ArrayList<Contact>> call = me.getContacts();

        call.enqueue(new Callback<ArrayList<Contact>>() {
            @Override
            public void onResponse(Call<ArrayList<Contact>> call, Response<ArrayList<Contact>> response) {
                sviKontakti = response.body();

                ListView mList = findViewById(R.id.listView);
                ContactListAdapter cla = new ContactListAdapter(getApplicationContext(),sviKontakti.size(),sviKontakti);
                mList.setAdapter(cla);

                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                    {
                        Intent setings = new Intent(ContactsActivity.this, ContactActivity.class);
                        setings.putExtra("CONTACT", sviKontakti.get(position));
                        startActivity(setings);
                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<Contact>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(nCtoggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
