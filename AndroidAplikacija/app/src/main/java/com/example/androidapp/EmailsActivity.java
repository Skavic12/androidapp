package com.example.androidapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.androidapp.Adapter.EmailListAdapter;
import com.example.androidapp.Retrofit.MessagesI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.Attachment;
import com.example.androidapp.model.EmailClass;
import com.example.androidapp.model.Objekti;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailsActivity extends AppCompatActivity {
    private DrawerLayout KlizniMnei;
    private ActionBarDrawerToggle ntoggle;
    private FloatingActionButton actionButton;
    private ArrayList<EmailClass> svePoruke;
    private ArrayList<Attachment> attachments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emails);
        attachments=new ArrayList<>();
        Toolbar tulbar = findViewById(R.id.toolbar_emails);
        setSupportActionBar(tulbar);

        getSupportActionBar().setTitle("Emails");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        KlizniMnei= (DrawerLayout) findViewById(R.id.emails_drawer);
        ntoggle = new ActionBarDrawerToggle(this,KlizniMnei,R.string.open,R.string.close);
        KlizniMnei.addDrawerListener(ntoggle);
        ntoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        actionButton = findViewById(R.id.emailsFAB);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EmailsActivity.this , CreateEmailActivity.class);
                startActivity(i);
                return;
            }
        });

        NavigationView navView = findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.emails_menu:
                        Intent em = new Intent(EmailsActivity.this,EmailsActivity.class);
                        startActivity(em);
                        break;
                    case R.id.folders_menu:
                        Intent fm = new Intent(EmailsActivity.this,FoldersActivity.class);
                        startActivity(fm);
                        break;
                    case R.id.contacts_menu:
                        Intent cm = new Intent(EmailsActivity.this,ContactsActivity.class);
                        startActivity(cm);
                        break;
                    case R.id.drafts_nav:
                        Intent c = new Intent(EmailsActivity.this,DraftActivity.class);
                        startActivity(c);
                        break;
                    case R.id.setting_menu:
                        Intent sm = new Intent(EmailsActivity.this,SettingsActivity.class);
                        startActivity(sm);
                        break;
                    case R.id.logout_nav:
                        SaveSharedPreference.getLoggedStatus(getApplicationContext());
                        break;

                }
                return  true;
            }

        });

        View headW = navView.getHeaderView(0);
        ImageView profil = headW.findViewById(R.id.proFilsHEd);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EmailsActivity.this , ProfileActivity.class);
                startActivity(i);
                return;
            }
        });

        MessagesI me = RetrofitC.getClient().create(MessagesI.class);
        Call<ArrayList<EmailClass>> call = me.getMessages();

        call.enqueue(new Callback<ArrayList<EmailClass>>() {
            @Override
            public void onResponse(Call<ArrayList<EmailClass>> call, Response<ArrayList<EmailClass>> response) {
                svePoruke = response.body();

                ListView mList = findViewById(R.id.listaEmailFolder);
                EmailListAdapter ela = new EmailListAdapter(getApplicationContext(),svePoruke.size(),svePoruke);
                mList.setAdapter(ela);

                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                    {
                        Intent setings = new Intent(EmailsActivity.this, EmailActivity.class);
                        setings.putExtra("MESSAGE", svePoruke.get(position));
                        startActivity(setings);
                    }
                });


            }

            @Override
            public void onFailure(Call<ArrayList<EmailClass>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }



    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.emails_menu,menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(ntoggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
