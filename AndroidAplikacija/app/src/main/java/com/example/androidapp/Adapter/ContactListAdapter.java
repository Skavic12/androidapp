package com.example.androidapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.androidapp.ContactActivity;
import com.example.androidapp.R;
import com.example.androidapp.model.Contact;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ContactListAdapter extends ArrayAdapter<Contact> {
    private Context context;
    private ArrayList<Contact> contacts;

    public ContactListAdapter(Context context, int resource, ArrayList<Contact> contacts) {
        super(context, resource, contacts);

        this.context = context;
        this.contacts = contacts;

    }

    @NonNull
    @Override
    public View getView(final int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        Contact c = contacts.get(position);


        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_view_layout_contacts, parent, false);

        }


        ImageView img = convertView.findViewById(R.id.image);
        TextView txtIme = convertView.findViewById(R.id.imeL);
        TextView txtPrezime = convertView.findViewById(R.id.prezimeL);
        TextView txtEmail = convertView.findViewById(R.id.emailL);

        txtIme.setText(c.getIme());
        txtPrezime.setText(c.getPrezime());
        txtEmail.setText(c.getEmail());

     //   String url = c.getDisplay().getPutanja();

        Picasso.with(context).load("https://www.w3schools.com/howto/img_avatar2.png").into(img);

        return convertView;
    }



}




