package com.example.androidapp.Retrofit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.androidapp.LoginActivity;
import com.example.androidapp.R;
import com.example.androidapp.model.Account;

public class SaveSharedPreference {

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setLoggedIn(Context context, Account a) {
        SharedPreferences.Editor editor = getPreferences(context).edit()
                .putString("username", a.getKorisnickoIme());
//        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.commit();
        Log.d("LOGIN",

                " useraname: " + a.getKorisnickoIme() +
                        " id: " + a.getId());
    }

    public static void getLoggedStatus(Context context) {
        SharedPreferences.Editor editor = getPreferences(context).edit().putString(context.getString(R.string.current_user_name), null);
        editor.commit();
        context.startActivity(new Intent(context, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


    public static void setRefreshrate(Context context, String refreshrate) {
        SharedPreferences.Editor editor = getPreferences(context).edit()
                .putString("refresh_rate", refreshrate);
    }

}
