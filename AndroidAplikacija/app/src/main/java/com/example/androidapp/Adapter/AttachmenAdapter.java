package com.example.androidapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidapp.R;
import com.example.androidapp.model.Attachment;

import java.util.ArrayList;
import java.util.List;

public class AttachmenAdapter extends ArrayAdapter<Attachment> {

    ArrayList<Attachment> attachments;
    Context mContext;
    public AttachmenAdapter(Context context , int textViewResourceId,ArrayList<Attachment> attachments){
        super(context,textViewResourceId,attachments);
        this.attachments = attachments;
        this.mContext = context;
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Attachment att = attachments.get(position);


        if(convertView == null)
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_attac,parent,false);


        TextView ime = (TextView)convertView.findViewById(R.id.name);
        // TextView tip = (TextView) listItem.findViewById(R.id.type);
        ime.setText( att.getIme());
        //  tip.setText( attachments.get(position).getTip());
        return convertView;
    }


}
