package com.example.androidapp.JavaMAPI;

import android.os.AsyncTask;

public class MailSenderAsyncTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object... arg0) {
            GMailSender sender = new GMailSender("test.ilkic@gmail.com", "testilija");
            try {
                sender.sendMail("This is Subject",
                        "This is Body",
                        "test.ilkic@gmail.com",
                        "test.ilkic@gmail.com");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
}
