package com.example.androidapp;

import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidapp.Adapter.ContactListAdapter;
import com.example.androidapp.Retrofit.ContactI;
import com.example.androidapp.Retrofit.DraftI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Contact;
import com.example.androidapp.model.EmailClass;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.androidapp.Notifications.CHANNEL_1_ID;

public class DraftCRActivity extends AppCompatActivity {
    private ArrayList<Contact> Kon;
    private NotificationManagerCompat notificationManager;
    private EditText editTextTitle;
    private EditText editTextEmail;
    private EditText editTextMessage;


    //  String[] kontakti = {"Ilija Ilkic/ilija@gmail.com", "Nenad SKavic/skava@gmail.com", "Marko Dzinovic/mare@gmail.com"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_cr);

        Toolbar toolbar = findViewById(R.id.toolbar_draftCR);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Draft Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent in = getIntent();
        EmailClass email= (EmailClass) in.getExtras().getSerializable("DRAFT");

        MultiAutoCompleteTextView od = findViewById(R.id.multiAutoCompleteTextViewDraft);
        EditText tema = findViewById(R.id.subject_primaoc_email_Draft);
        EditText sadrzaj = findViewById(R.id.content_primaoc_email_Draft);

        if(email.getTo().getEmail() == null){
            od.setText("");
        }else{
            od.setText(email.getTo().getEmail());
        }
        if(email.getTema().equals("a")){
            tema.setText("");
        }else{
            tema.setText(email.getTema());
        }
        if(email.getSadrzajPor().equals("a")){
            sadrzaj.setText("");
        }else{
            sadrzaj.setText(email.getSadrzajPor());
        }

        ContactI me = RetrofitC.getClient().create(ContactI.class);
        Call<ArrayList<Contact>> call = me.getContacts();

        call.enqueue(new Callback<ArrayList<Contact>>() {
            @Override
            public void onResponse(Call<ArrayList<Contact>> call, Response<ArrayList<Contact>> response) {
                Kon = response.body();


                ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>
                        (getApplicationContext(), android.R.layout.select_dialog_item, Kon);


                MultiAutoCompleteTextView mactv = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextViewDraft);
                mactv.setThreshold(1);//will start working from first character
                mactv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                mactv.setTextColor(Color.RED);
                mactv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

            }

            @Override
            public void onFailure(Call<ArrayList<Contact>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        Toast.makeText(getApplicationContext(),od.getText().toString(),Toast.LENGTH_LONG).show();

        notificationManager = NotificationManagerCompat.from(this);

        editTextTitle = findViewById(R.id.subject_primaoc_email_Draft);
        editTextMessage = findViewById(R.id.content_primaoc_email_Draft);
        editTextEmail = findViewById(R.id.multiAutoCompleteTextViewDraft);

    }
    public void sendOnChannel1() {
        String title = editTextTitle.getText().toString();
        String email = editTextEmail.getText().toString();
        String message = editTextMessage.getText().toString();

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_account)
                .setContentTitle(email)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        notificationManager.notify(1234, notification);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.draft_menu, menu);
        return  true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.posalji_email_draft:

                Intent in = getIntent();
                EmailClass email= (EmailClass) in.getExtras().getSerializable("DRAFT");
                int id = email.getId();

                MultiAutoCompleteTextView od = findViewById(R.id.multiAutoCompleteTextViewDraft);
                EditText tema = findViewById(R.id.subject_primaoc_email_Draft);
                EditText sadrzaj = findViewById(R.id.content_primaoc_email_Draft);

                if(od.getText().toString().trim().equals("")){
                    Toast.makeText(this,"Unesite kome saljete poruku",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(tema.getText().toString().trim().equals("")){
                    Toast.makeText(this,"Unesite temu",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(sadrzaj.getText().toString().equals("")){
                    Toast.makeText(this,"Unesite sadrzaj",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(!od.getText().toString().trim().contains("@gmail.com")){
                    Toast.makeText(this,"Unesite ispravnu email adresu",Toast.LENGTH_LONG).show();
                    return true;
                }

                DraftI izm = RetrofitC.getClient().create(DraftI.class);
                Call<EmailClass> emm = izm.updateMessages(id,od.getText().toString().trim(),tema.getText().toString().trim(),sadrzaj.getText().toString().trim());
                emm.enqueue(new Callback<EmailClass>() {
                    @Override
                    public void onResponse(Call<EmailClass> call, Response<EmailClass> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                        sendOnChannel1();

                        Toast.makeText(getApplicationContext(),"Poslali ste poruku",Toast.LENGTH_LONG).show();

                        Intent i12 = new Intent(DraftCRActivity.this,DraftActivity.class);
                        startActivity(i12);
                    }

                    @Override
                    public void onFailure(Call<EmailClass> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });

                return true;


            case R.id.odustani_draft:

                Intent i = getIntent();
                EmailClass e= (EmailClass) i.getExtras().getSerializable("DRAFT");

                DraftI service = RetrofitC.getClient().create(DraftI.class);
                Call<Void> call = service.brisanjeDraft(e.getId());
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {

                        Toast.makeText(DraftCRActivity.this, "Brisanje Email",Toast.LENGTH_SHORT).show();
                        Intent i12 = new Intent(DraftCRActivity.this,DraftActivity.class);
                        startActivity(i12);


                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(DraftCRActivity.this, "GRESKA", Toast.LENGTH_SHORT).show();
                    }
                });

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
