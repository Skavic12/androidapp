package com.example.androidapp.Retrofit;

import com.example.androidapp.model.EmailClass;
import com.example.androidapp.model.Folder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DraftI {

    @GET("drafts")
    Call<ArrayList<EmailClass>> getDrafts();

    @POST("draft/{toD}/{temaD}/{sadrzajD}")
    Call<EmailClass> draftPoruka(@Path("toD") String to, @Path("temaD") String tema,
                                 @Path("sadrzajD") String sadrzaj);


    @PUT("drafts/{id}/{to}/{tema}/{sadrzaj}")
    Call<EmailClass> updateMessages(@Path("id") int id, @Path("to") String name, @Path("tema") String condition,
                               @Path("sadrzaj") String operation);


    @DELETE("drafts/{id}")
    Call<Void> brisanjeDraft(@Path("id") int id);

}
