package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.androidapp.Retrofit.FoldersI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Folder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditFolder extends AppCompatActivity {

    RadioGroup rGcCon ;
    RadioButton rBcCon;

    RadioGroup rGoOper ;
    RadioButton rBoOper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_folder);

        Toolbar tol = findViewById(R.id.toolbar_create_folderzz);
        setSupportActionBar(tol);

        getSupportActionBar().setTitle("Izmena");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent izFoldersa = getIntent();
        Folder f = (Folder) izFoldersa.getExtras().getSerializable("IZMENAFOLDER");


        EditText etIme = findViewById(R.id.novo_ime_folderaz);
        etIme.setText(f.getIme());

        switch (f.getPravila().getCondition()){
            case TO:
                rBcCon = findViewById(R.id.radio_TOz);
                rBcCon.setChecked(true);
                break;
            case FROM:
                rBcCon = findViewById(R.id.radio_FROMz);
                rBcCon.setChecked(true);
                break;
            case SUBJECT:
                rBcCon = findViewById(R.id.radio_SUBJECTz);
                rBcCon.setChecked(true);
                break;
            case CC:
                rBcCon = findViewById(R.id.radio_CCz);
                rBcCon.setChecked(true);
                break;
        }


        switch (f.getPravila().getOperation()){
            case MOVE:
                rBoOper = findViewById(R.id.radio_MOVEz);
                rBoOper.setChecked(true);
                break;
            case COPY:
                rBoOper = findViewById(R.id.radio_COPYz);
                rBoOper.setChecked(true);
                break;
            case DELETE:
                rBoOper = findViewById(R.id.radio_DELETEz);
                rBoOper.setChecked(true);
                break;
        }


    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.editfoldermenu, menu);
        return  true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Kreiraj_folderz:

                Intent izFoldersa = getIntent();
                Folder f = (Folder) izFoldersa.getExtras().getSerializable("IZMENAFOLDER");

                EditText etIme = findViewById(R.id.novo_ime_folderaz);
                int id = f.getId();
                FoldersI me = RetrofitC.getClient().create(FoldersI.class);



                rGcCon = findViewById(R.id.conditionz);

                int radioId = rGcCon.getCheckedRadioButtonId();

                rBcCon = findViewById(radioId);

                rGoOper = findViewById(R.id.operationz);

                int radioId1 = rGoOper.getCheckedRadioButtonId();

                rBoOper = findViewById(radioId1);

                if(etIme.getText().toString().trim().equals("")){
                    Toast.makeText(this,"Unesite naziv foldera",Toast.LENGTH_LONG).show();
                    return true;
                }


                Call<Folder> call = me.updateFolders(id,etIme.getText().toString(),rBcCon.getText().toString(),rBoOper.getText().toString());

                call.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });

                Toast.makeText(this, "Kreirali ste folder",Toast.LENGTH_SHORT).show();
                return true;


            case R.id.odustani_kreiranje_folderaz:
                Toast.makeText(this, "Odustali ste od kreiranja foldera",Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void checkCondition(View view) {

        rGcCon = findViewById(R.id.conditionz);

        int radioId = rGcCon.getCheckedRadioButtonId();

        rBcCon = findViewById(radioId);

        Toast.makeText(EditFolder.this,"ovo je" + rBcCon.getText().toString(),Toast.LENGTH_LONG).show();
    }
    public void checkOperation(View view) {

        rGoOper = findViewById(R.id.operationz);

        int radioId = rGoOper.getCheckedRadioButtonId();

        rBoOper = findViewById(radioId);

        Toast.makeText(EditFolder.this,"ovo je" + rBoOper.getText().toString(),Toast.LENGTH_LONG).show();
    }
}
