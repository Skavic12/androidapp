package com.example.androidapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidapp.Retrofit.SaveSharedPreference;

public class ProfileActivity extends AppCompatActivity {

    private DrawerLayout meniPro;
    private ActionBarDrawerToggle nPtoggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.profileToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Profile");


        SharedPreferences share = SaveSharedPreference.getPreferences(getApplicationContext());

        String userName = share.getString("username","abcd");

        TextView userNameText = (TextView) findViewById(R.id.profile_ime);
        TextView passwordText = (TextView) findViewById(R.id.profile_prezime);
        userNameText.setText(userName);
        passwordText.setText(userName);




        meniPro= (DrawerLayout) findViewById(R.id.profile_drawer);
        nPtoggle = new ActionBarDrawerToggle(this,meniPro,R.string.open,R.string.close);
        meniPro.addDrawerListener(nPtoggle);
        nPtoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navView = findViewById(R.id.nav_view_profile);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.emails_menu:
                        Intent em = new Intent(ProfileActivity.this,EmailsActivity.class);
                        startActivity(em);
                        break;
                    case R.id.folders_menu:
                        Intent fm = new Intent(ProfileActivity.this,FoldersActivity.class);
                        startActivity(fm);
                        break;
                    case R.id.contacts_menu:
                        Intent cm = new Intent(ProfileActivity.this,ContactsActivity.class);
                        startActivity(cm);
                        break;
                    case R.id.drafts_nav:
                        Intent c = new Intent(ProfileActivity.this,DraftActivity.class);
                        startActivity(c);
                        break;
                    case R.id.setting_menu:
                        Intent sm = new Intent(ProfileActivity.this,SettingsActivity.class);
                        startActivity(sm);
                        break;
                    case R.id.logout_nav:
                        SaveSharedPreference.getLoggedStatus(getApplicationContext());
                        break;
                }
                return  true;
            }

        });

        View headW = navView.getHeaderView(0);
        ImageView profil = headW.findViewById(R.id.proFilsHEd);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileActivity.this , EditProfilActivity.class);
                startActivity(i);
                return;
            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.profile_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(nPtoggle.onOptionsItemSelected(item)){
            return true;
        }
        switch (item.getItemId()){

            case R.id.Edit_profil_dugme:
                Intent ed = new Intent(ProfileActivity.this,EditProfilActivity.class);
                startActivity(ed);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
