package com.example.androidapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Folder implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("ime")
    private String ime;
    @SerializedName("porukeFoldera")
    private ArrayList<EmailClass> porukeFoldera;
    @SerializedName("folderi")
    private ArrayList<Folder> folderi;
    @SerializedName("pravila")
    private Rule pravila;


    public Folder() {
        super();
    }

    public Folder(int id, String ime, ArrayList<EmailClass> porukeFoldera, ArrayList<Folder> folderi, Rule pravila) {
        this.id = id;
        this.ime = ime;
        this.porukeFoldera = porukeFoldera;
        this.folderi = folderi;
        this.pravila = pravila;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public ArrayList<EmailClass> getPorukeFoldera() {
        return porukeFoldera;
    }

    public void setPorukeFoldera(ArrayList<EmailClass> porukeFoldera) {
        this.porukeFoldera = porukeFoldera;
    }

    public ArrayList<Folder> getFolderi() {
        return folderi;
    }

    public void setFolderi(ArrayList<Folder> folderi) {
        this.folderi = folderi;
    }

    public Rule getPravila() {
        return pravila;
    }

    public void setPravila(Rule pravila) {
        this.pravila = pravila;
    }
}
