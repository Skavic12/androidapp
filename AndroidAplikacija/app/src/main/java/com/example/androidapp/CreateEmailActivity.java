package com.example.androidapp;

import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidapp.JavaMAPI.GMailSender;
import com.example.androidapp.JavaMAPI.MailSenderAsyncTask;
import com.example.androidapp.Retrofit.ContactI;
import com.example.androidapp.Retrofit.DraftI;
import com.example.androidapp.Retrofit.MessagesI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Contact;
import com.example.androidapp.model.EmailClass;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.androidapp.Notifications.CHANNEL_1_ID;

public class CreateEmailActivity extends AppCompatActivity {

    private ArrayList<Contact> Kon;

//    String[] kontakti = {"Ilija Ilkic/ilija@gmail.com", "Nenad SKavic/skava@gmail.com", "Marko Dzinovic/mare@gmail.com"};
    private NotificationManagerCompat notificationManager;
    private EditText editTextTitle;
    private EditText editTextMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_email);

        Toolbar toolbar = findViewById(R.id.toolbar_create_Email);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create Email");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ContactI me = RetrofitC.getClient().create(ContactI.class);
        Call<ArrayList<Contact>> call = me.getContacts();

        call.enqueue(new Callback<ArrayList<Contact>>() {
            @Override
            public void onResponse(Call<ArrayList<Contact>> call, Response<ArrayList<Contact>> response) {
                Kon = response.body();


                ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>
                        (getApplicationContext(), android.R.layout.select_dialog_item, Kon);



                //Getting the instance of MultiAutoCompleteTextView
                MultiAutoCompleteTextView mactv = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextView);
                mactv.setThreshold(1);//will start working from first character
                mactv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                mactv.setTextColor(Color.RED);
                mactv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

                MultiAutoCompleteTextView mactvbcc = (MultiAutoCompleteTextView) findViewById(R.id.bccmultiAutoCompleteTextView);
                mactvbcc.setThreshold(1);//will start working from first character
                mactvbcc.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                mactvbcc.setTextColor(Color.RED);
                mactvbcc.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

                MultiAutoCompleteTextView mactvcc = (MultiAutoCompleteTextView) findViewById(R.id.ccmultiAutoCompleteTextView);
                mactvcc.setThreshold(1);//will start working from first character
                mactvcc.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                mactvcc.setTextColor(Color.RED);
                mactvcc.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());



            }

            @Override
            public void onFailure(Call<ArrayList<Contact>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        notificationManager = NotificationManagerCompat.from(this);

        editTextTitle = findViewById(R.id.subject_primaoc_email);
        editTextMessage = findViewById(R.id.content_primaoc_email);

    }

    private void sendGMail() {
       new MailSenderAsyncTask().execute();
    }
    public void sendOnChannel1() {
        String title = editTextTitle.getText().toString();
        String message = editTextMessage.getText().toString();

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_account)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        notificationManager.notify(1234, notification);
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.posalji_email:

                MultiAutoCompleteTextView od = findViewById(R.id.multiAutoCompleteTextView);
                MultiAutoCompleteTextView bcc = findViewById(R.id.bccmultiAutoCompleteTextView);
                MultiAutoCompleteTextView cc = findViewById(R.id.ccmultiAutoCompleteTextView);
                EditText temaE = findViewById(R.id.subject_primaoc_email);
                EditText sadrzajE = findViewById(R.id.content_primaoc_email);

                String primalac = od.getText().toString();
                String primalacbcc = bcc.getText().toString();
                String primalaccc = cc.getText().toString();
                String tema = temaE.getText().toString();
                String sadrzaj = sadrzajE.getText().toString();


                if(primalac.trim().equals("")){
                    Toast.makeText(this,"Unesite kome saljete",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(tema.trim().equals("")){
                    //   temaE.setError("Popunite temuu");
                    Toast.makeText(this,"Unesite temu",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(sadrzaj.trim().equals("")){
                    Toast.makeText(this,"Unesite sadrzaj poruke",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(!primalac.trim().contains("@gmail.com")){
                    Toast.makeText(this,"Unesite ispravnu email adresu",Toast.LENGTH_LONG).show();
                    return true;
                }


                MessagesI me = RetrofitC.getClient().create(MessagesI.class);
                Call<EmailClass> call = me.dodacanjeEmail(primalac,tema,sadrzaj);

                call.enqueue(new Callback<EmailClass>() {
                    @Override
                    public void onResponse(Call<EmailClass> call, Response<EmailClass> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        } else {
                            sendOnChannel1();
                            sendGMail();
                        }
                    }

                    @Override
                    public void onFailure(Call<EmailClass> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });

                Intent i = new Intent(CreateEmailActivity.this,EmailsActivity.class);
                startActivity(i);

                Toast.makeText(this, "Poslali ste email",Toast.LENGTH_SHORT).show();
                return true;


            case R.id.odustani_slanja_email:

                AutoCompleteTextView primalacD = findViewById(R.id.multiAutoCompleteTextView);
                EditText temaD = findViewById(R.id.subject_primaoc_email);
                EditText sadrzajD = findViewById(R.id.content_primaoc_email);

                String primaacS = primalacD.getText().toString();
                String temaS = temaD.getText().toString();
                String sadrzajS = sadrzajD.getText().toString();


                if(temaS.equals("") && sadrzajS.equals("") && primaacS.trim().equals("")){

                    Toast.makeText(this, "Odustali ste od slanja emaila",Toast.LENGTH_SHORT).show();

                    Intent i12 = new Intent(CreateEmailActivity.this,EmailsActivity.class);
                    startActivity(i12);
                    return true;
                }

                if(!temaS.trim().equals("") || !sadrzajS.trim().equals("") || !primaacS.trim().equals("")){

                    if(temaS.trim().equals("")){
                        temaS = "a";
                    }
                    if(sadrzajS.trim().equals("")){
                        sadrzajS = "a";
                    }
                    if(primaacS.trim().equals("")){
                        primaacS = "a";
                    }

                    DraftI me1 = RetrofitC.getClient().create(DraftI.class);
                    Call<EmailClass> call1 = me1.draftPoruka(primaacS,temaS,sadrzajS);

                    call1.enqueue(new Callback<EmailClass>() {
                        @Override
                        public void onResponse(Call<EmailClass> call, Response<EmailClass> response) {
                            if (!response.isSuccessful()){
                                Log.i("STATUS CODE", Integer.toString(response.code()));
                                return;
                            }
                        }

                        @Override
                        public void onFailure(Call<EmailClass> call, Throwable t) {
                            Log.i("STACK","TRACE");
                            t.printStackTrace();
                        }
                    });

                    Intent i1 = new Intent(CreateEmailActivity.this,EmailsActivity.class);
                    startActivity(i1);

                    Toast.makeText(this,"Poruka sacuvana u draft-u",Toast.LENGTH_LONG).show();
                    return true;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.create_mail_menu, menu);
        return  true;
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
