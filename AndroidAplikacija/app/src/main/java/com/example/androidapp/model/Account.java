package com.example.androidapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Account implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("korisnickoIme")
    private String korisnickoIme;
    @SerializedName("sifra")
    private String sifra;
    @SerializedName("smtp")
    private String smtp;
    @SerializedName("imap")
    private String imap;
    @SerializedName("pop3")
    private String pop3;
    @SerializedName("svePoruke")
    private ArrayList<EmailClass> svePoruke;

    public Account() {
        super();
    }

    public Account(int id, String korisnickoIme, String sifra, String smtp, String imap, String pop3, ArrayList<EmailClass> svePoruke) {
        this.id = id;
        this.korisnickoIme = korisnickoIme;
        this.sifra = sifra;
        this.smtp = smtp;
        this.imap = imap;
        this.pop3 = pop3;
        this.svePoruke = svePoruke;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getImap() {
        return imap;
    }

    public void setImap(String imap) {
        this.imap = imap;
    }

    public String getPop3() {
        return pop3;
    }

    public void setPop3(String pop3) {
        this.pop3 = pop3;
    }

    public ArrayList<EmailClass> getSvePoruke() {
        return svePoruke;
    }

    public void setSvePoruke(ArrayList<EmailClass> svePoruke) {
        this.svePoruke = svePoruke;
    }
}
