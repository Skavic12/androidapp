package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.androidapp.Retrofit.FoldersI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Folder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFolderActivity extends AppCompatActivity {

    RadioGroup rGcCon ;
    RadioButton rBcCon;

    RadioGroup rGoOper ;
    RadioButton rBoOper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);

        Toolbar toolbar = findViewById(R.id.toolbar_create_folder);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create Folder");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.create_folder_menu, menu);
        return  true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Kreiraj_folder:
                EditText etIme = findViewById(R.id.novo_ime_foldera);
                String ime = etIme.getText().toString();
                FoldersI me = RetrofitC.getClient().create(FoldersI.class);

                if(ime.trim().equals("")){
                    Toast.makeText(this,"Unesite naziv foldera",Toast.LENGTH_LONG).show();
                    return true;
                }


                rGcCon = findViewById(R.id.condition);

                int radioId = rGcCon.getCheckedRadioButtonId();

                rBcCon = findViewById(radioId);

                rGoOper = findViewById(R.id.operation);

                int radioId1 = rGoOper.getCheckedRadioButtonId();

                rBoOper = findViewById(radioId1);



                Call<Folder> call = me.createFolder(ime,rBcCon.getText().toString(),rBoOper.getText().toString());

                call.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });

                Intent i = new Intent(CreateFolderActivity.this,FoldersActivity.class);
                startActivity(i);

                Toast.makeText(this, "Poslali ste email",Toast.LENGTH_SHORT).show();
                return true;


            case R.id.odustani_kreiranje_foldera:
                Intent i2 = new Intent(CreateFolderActivity.this,FoldersActivity.class);
                startActivity(i2);
                Toast.makeText(this, "Odustali ste od kreiranja foldera",Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void checkCondition(View view) {

        rGcCon = findViewById(R.id.condition);

        int radioId = rGcCon.getCheckedRadioButtonId();

        rBcCon = findViewById(radioId);

        Toast.makeText(CreateFolderActivity.this,"ovo je" + rBcCon.getText().toString(),Toast.LENGTH_LONG).show();
    }
    public void checkOperation(View view) {

        rGoOper = findViewById(R.id.operation);

        int radioId = rGoOper.getCheckedRadioButtonId();

        rBoOper = findViewById(radioId);

        Toast.makeText(CreateFolderActivity.this,"ovo je" + rBoOper.getText().toString(),Toast.LENGTH_LONG).show();
    }
}
