package com.example.androidapp.Retrofit;

import com.example.androidapp.model.Account;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AccountService {

    @GET("login/{username}/{password}")
    Call<Account> login(@Path("username") String username,@Path("password")String password);


    @PUT("account/{id}/{username}/{password}/{pop3Imap}/{smtp}")
    Call<Account> editAccount(@Path("id") int id, @Path("username") String username,
                              @Path("password") String password, @Path("pop3Imap") String pop3Imap,
                              @Path("smtp") String smtp );


    @GET("accounts/{username}")
    Call<Account> getAccount(@Path("username") String username);
}
