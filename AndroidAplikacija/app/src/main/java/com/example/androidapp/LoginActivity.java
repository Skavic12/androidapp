package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.androidapp.Retrofit.AccountService;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.Account;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {
    EditText edtUsername;
    EditText edtPassword;
    Button btnlogin;
    AccountService service;
    private ArrayList<Account> accounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtUsername = (EditText) findViewById(R.id.userName);
        edtPassword = (EditText) findViewById(R.id.password);

        Button btnLogin = (Button) findViewById(R.id.openEmails);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();

                Retrofit retrofit = RetrofitC.getClient();
                AccountService services = retrofit.create(AccountService.class);
                Call<Account> call = services.login(username,password);
                call.enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        if(response.isSuccessful()){

                            Account a= response.body();

                            SaveSharedPreference.setLoggedIn(getApplicationContext(), a);
                            SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");
                            //login start main activity
                            Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |FLAG_ACTIVITY_CLEAR_TASK);
//                       intent.putExtra("username", username);
                            startActivity(intent);
                            finish();
                        }
                        if(!response.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Nije dobra sifra ili korisnicko ime", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("dadsda","fdsd");
                        Log.d("sdfdsfsd",t.getMessage());
                        Log.d("sdfdsfsd",t.getStackTrace().toString());

                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
