package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidapp.Adapter.AttachmenAdapter;
import com.example.androidapp.Retrofit.MessagesI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Attachment;
import com.example.androidapp.model.EmailClass;
import com.example.androidapp.model.Folder;
import com.example.androidapp.model.Objekti;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailActivity extends AppCompatActivity {
    private List<Attachment> attachments;
    private EmailClass email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        Toolbar toolbar = findViewById(R.id.emailToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Email");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent in = getIntent();
        EmailClass email= (EmailClass) in.getExtras().getSerializable("MESSAGE");

        if(email.isSeen() == false) {

            MessagesI me = RetrofitC.getClient().create(MessagesI.class);
            Call<EmailClass> call = me.izmenaMe(email.getId());

            call.enqueue(new Callback<EmailClass>() {
                @Override
                public void onResponse(Call<EmailClass> call, Response<EmailClass> response) {
                    if (!response.isSuccessful()){
                        Log.i("STATUS CODE", Integer.toString(response.code()));
                        return;
                    }
                }

                @Override
                public void onFailure(Call<EmailClass> call, Throwable t) {
                    Log.i("STACK","TRACE");
                    t.printStackTrace();
                }
            });
        }
        TextView od = findViewById(R.id.odEmail);
        TextView tema = findViewById(R.id.temaEmail);
        TextView sadrzaj = findViewById(R.id.sadrzajEmail);

        od.setText(email.getFrom().getEmail());
        tema.setText(email.getTema());
        sadrzaj.setText(email.getSadrzajPor());

        ListView mList = findViewById(R.id.listAtachmantEmail);
        AttachmenAdapter ela = new AttachmenAdapter(getApplicationContext(),email.getPrilog().size(),email.getPrilog());
        mList.setAdapter(ela);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.email_menu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.brisanjeEmail:
                Intent in = getIntent();
                EmailClass email= (EmailClass) in.getExtras().getSerializable("MESSAGE");

                MessagesI service = RetrofitC.getClient().create(MessagesI.class);
                Call<Void> call = service.brisanjeEmail(email.getId());
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Intent i=new Intent(EmailActivity.this, EmailsActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(EmailActivity.this, "GRESKA", Toast.LENGTH_SHORT).show();
                    }
                });
                Toast.makeText(this, "Brisanje Email",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.saveEmail:
                Toast.makeText(this, "Sacuvati Email",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.replayEmail:
                Toast.makeText(this, "Odgovori Email",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.replayToAllEmail:
                Toast.makeText(this, "Odgovori svima Email",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.forwardEmail:
                Toast.makeText(this, "Prosledi Email",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }



    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
