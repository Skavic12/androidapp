package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidapp.Adapter.EmailListAdapter;
import com.example.androidapp.Adapter.FolderListAdapter;
import com.example.androidapp.Retrofit.FoldersI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Contact;
import com.example.androidapp.model.EmailClass;
import com.example.androidapp.model.Folder;
import com.example.androidapp.model.Objekti;
import com.example.androidapp.model.Rule;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FolderActivity extends AppCompatActivity {

    private  ArrayList<EmailClass> emails ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        Toolbar Ftoolbar = findViewById(R.id.FolderToolbar);
        setSupportActionBar(Ftoolbar);

        getSupportActionBar().setTitle("Folder");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent izFoldersa = getIntent();


        Folder f = (Folder) izFoldersa.getExtras().getSerializable("FOLDER");
        Rule r = f.getPravila();

        if(f.getPorukeFoldera() == null && f.getFolderi() == null){

            TextView rul = findViewById(R.id.ruleFolderD);
            TextView imef = findViewById(R.id.imeFoldera);
            imef.setText(f.getIme());
            rul.setText(r.getCondition().toString() + "," + r.getOperation().toString());

        }else {

            TextView rul = findViewById(R.id.ruleFolderD);
            TextView imef = findViewById(R.id.imeFoldera);
            imef.setText(f.getIme());
            rul.setText(r.getCondition().toString() + "," + r.getOperation().toString());


            ListView mList = findViewById(R.id.listaEmailFolder);
            emails = f.getPorukeFoldera();
            EmailListAdapter ela = new EmailListAdapter(this,R.layout.layout_email_list,emails);
            mList.setAdapter(ela);

            mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                {
                    Intent setings = new Intent(FolderActivity.this, EmailActivity.class);
                    setings.putExtra("MESSAGE", emails.get(position));
                    startActivity(setings);
                }
            });

        }





    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater infd = getMenuInflater();
        infd.inflate(R.menu.folder_menu, menu);
        return  true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.izmenaFoldera:

                Intent izFoldersa = getIntent();


                Folder f = (Folder) izFoldersa.getExtras().getSerializable("FOLDER");
                if(f.getId() == 2){
                    Toast.makeText(this,"Ovaj folder se ne moze izmeniti",Toast.LENGTH_LONG).show();
                    return true;
                }
                Intent profile = new Intent(FolderActivity.this, EditFolder.class);
                profile.putExtra("IZMENAFOLDER", f);
                startActivity(profile);
                Toast.makeText(this, "Izmena Foldera",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.brisanjeFoldera:
                Intent fol = getIntent();
                Folder folder = (Folder) fol.getExtras().getSerializable("FOLDER");

                if(folder.getId() == 1 || folder.getId() == 2){
                    Toast.makeText(FolderActivity.this,"Ovaj folder se ne moze obrisati",Toast.LENGTH_LONG).show();
                    return true;
                }
                FoldersI service = RetrofitC.getClient().create(FoldersI.class);
                Call<Void> call = service.brisanjeFoldera(folder.getId());
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Intent i=new Intent(FolderActivity.this, FoldersActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(FolderActivity.this, "GRESKA", Toast.LENGTH_SHORT).show();
                    }
                });
                Toast.makeText(this, "Izbrisan Folder",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();



    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}