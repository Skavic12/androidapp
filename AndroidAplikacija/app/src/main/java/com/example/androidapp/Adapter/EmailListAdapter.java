package com.example.androidapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidapp.EmailActivity;
import com.example.androidapp.FolderActivity;
import com.example.androidapp.R;
import com.example.androidapp.model.EmailClass;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class EmailListAdapter extends ArrayAdapter<EmailClass> {

    private Context context;
    private ArrayList<EmailClass> emails;

    public EmailListAdapter(Context context, int resource, ArrayList<EmailClass> emails) {
        super(context, resource, emails);

        this.context = context;
        this.emails = emails;

    }


    @NonNull
    @Override
    public View getView( int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        EmailClass m = emails.get(position);



        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_email_list, parent, false);

        }


        TextView txtKor = convertView.findViewById(R.id.korisnikEmailF);
        TextView txtTema = convertView.findViewById(R.id.as);
        TextView txtSadrzaj = convertView.findViewById(R.id.as1);
        TextView txtVreme = convertView.findViewById(R.id.vremePor);

        //+ "," + m.getTo().getEmail()

        if(m.isSeen() == false){

            String sa =m.getFrom().getEmail()+","+ m.getTo().getEmail();

            SpannableString kor = new SpannableString(sa);
            StyleSpan boldItalicSp = new StyleSpan(Typeface.BOLD_ITALIC);

            kor.setSpan(boldItalicSp, 0,sa.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtKor.setText(kor);


            String tema = m.getTema();
            if(tema.length() > 20 ){
                tema = tema.substring(0,16) + "...";
            }

            SpannableString temass = new SpannableString(tema);
            StyleSpan boldItalicSpan = new StyleSpan(Typeface.BOLD_ITALIC);

            temass.setSpan(boldItalicSpan, 0, tema.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtTema.setText(temass);

            String sadrzaj= m.getSadrzajPor();
            if(sadrzaj.length() > 35 ){
                sadrzaj = sadrzaj.substring(0,31) + "...";
            }
            SpannableString sadrzajss = new SpannableString(sadrzaj);
            StyleSpan boldItalicS = new StyleSpan(Typeface.BOLD_ITALIC);

            sadrzajss.setSpan(boldItalicS, 0, sadrzaj.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtSadrzaj.setText(sadrzajss);

            Date vre  = m.getVreme();
            TimeZone tz = TimeZone.getTimeZone("UTC");
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            df.setTimeZone(tz);

            String novoVreme = df.format(vre);

            txtVreme.setText(novoVreme);


            return convertView;

        }

        txtKor.setText(m.getFrom().getEmail()+","+ m.getTo().getEmail());


        String tema = m.getTema();
        if(tema.length() > 20 ){
            tema = tema.substring(0,16) + "...";
        }
        txtTema.setText(tema);

        String sadrzaj= m.getSadrzajPor();
        if(sadrzaj.length() > 35 ){
            sadrzaj = sadrzaj.substring(0,31) + "...";
        }
        txtSadrzaj.setText(sadrzaj);

        Date vre  = m.getVreme();
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        df.setTimeZone(tz);

        String novoVreme = df.format(vre);

        txtVreme.setText(novoVreme);



        return convertView;
    }

}
