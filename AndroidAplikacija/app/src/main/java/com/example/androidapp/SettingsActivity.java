package com.example.androidapp;

import android.content.Intent;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.util.SortedList;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.example.androidapp.Adapter.EmailListAdapter;
import com.example.androidapp.Retrofit.MessagesI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.EmailClass;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends PreferenceActivity {
    private static int vreme = 500;
    private ArrayList<EmailClass> messages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);


        final CheckBoxPreference desc=(CheckBoxPreference) findPreference("sortiranje_opadajuce");
        desc.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(desc.isChecked()){
                    MessagesI service = RetrofitC.getClient().create(MessagesI.class);
                    Call<ArrayList<EmailClass>> call = service.getAllMessagesDesc();
                    call.enqueue(new Callback<ArrayList<EmailClass>>() {
                        @Override
                        public void onResponse(Call<ArrayList<EmailClass>> call, Response<ArrayList<EmailClass>> response) {
                            messages= response.body();
                            ListView listView =(ListView)findViewById(R.id.listaEmailFolder);
                            EmailListAdapter mAdapter=new EmailListAdapter(getApplicationContext(),messages.size(),messages);
                            Intent folders = new Intent(SettingsActivity.this, EmailsActivity.class);
                            startActivity(folders);
                       }

                        @Override
                        public void onFailure(Call<ArrayList<EmailClass>> call, Throwable t) {
                            Log.d("dadsda","fdsd");
                            Log.d("asssss",t.getMessage());
                            Log.d("dgdh",t.getStackTrace().toString());
                            Toast.makeText(SettingsActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                return true;
            }
        });
        final CheckBoxPreference asc=(CheckBoxPreference) findPreference("sortiranje_rastuce");
        asc.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(asc.isChecked()){
                    MessagesI service = RetrofitC.getClient().create(MessagesI.class);
                    Call<ArrayList<EmailClass>> call = service.getAllMessagesAsc();
                    call.enqueue(new Callback<ArrayList<EmailClass>>() {
                        @Override
                        public void onResponse(Call<ArrayList<EmailClass>> call, Response<ArrayList<EmailClass>> response) {

                            messages = response.body();

                            ListView listView =(ListView)findViewById(R.id.listaEmailFolder);
                            EmailListAdapter mAdapter=new EmailListAdapter(getApplicationContext(),messages.size(),messages);
//
                            Intent folders = new Intent(SettingsActivity.this, EmailsActivity.class);
                            startActivity(folders);

                        }

                        @Override
                        public void onFailure(Call<ArrayList<EmailClass>> call, Throwable t) {
//                progressDoalog.dismiss();
                            Log.d("dadsda","fdsd");
                            Log.d("asssss",t.getMessage());
                            Log.d("dgdh",t.getStackTrace().toString());
                            Toast.makeText(SettingsActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                return true;
            }
        });
        final CheckBoxPreference mEnable_icon = (CheckBoxPreference) findPreference("pref_sync");
        mEnable_icon.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference preference) {

                if(false==mEnable_icon.isChecked()){
                    vreme =-1;}
                Log.d("animalsList"," animalsList    --"+vreme);


                Log.d("cech"," animalsList    --"+mEnable_icon.isChecked());


                if(true==mEnable_icon.isChecked()){


                    final ListPreference animalsList = (ListPreference) findPreference( "ListPreferencAndroid");

                    animalsList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                        public boolean onPreferenceChange(Preference preference, Object newValue) {




                            if (animalsList.getValue().equals("1")) {
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");

                                vreme=6000;




                            }  else if (animalsList.getValue().equals("10")){
                                vreme=1500;
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "10");
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());







                            } else if (animalsList.getValue().equals("30")) {
                                vreme=2000;
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "30");



                            } else if (String.valueOf(animalsList).equals("Lista 60 min")) {
                                vreme=2000;
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme);
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "60");


                            } else if (String.valueOf(animalsList).equals("-1")) {
                                vreme=-1;
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme);

                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");


                            } else if (animalsList.getValue().equals("60")) {
                                vreme=-1;
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());


                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");


                            } else {
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());
                                vreme=500;
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");
                            }

                            return true;
                        }
                    });


                }


                Log.d("prikaz"," ListPreference ++2++++"+vreme);




                return true;




            }


        });




    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
