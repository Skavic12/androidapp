package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.androidapp.Retrofit.ContactI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Contact;
import com.example.androidapp.model.Objekti;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.contactToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Contact");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent in = getIntent();
        Contact contact = (Contact) in.getExtras().getSerializable("CONTACT");


        EditText imeK = findViewById(R.id.imeKontantk);
        EditText prezimeK = findViewById(R.id.prezimeKontakt);
        EditText emailK = findViewById(R.id.emailKontakt);

        imeK.setText(contact.getIme());
        prezimeK.setText(contact.getPrezime());
        emailK.setText(contact.getEmail());


    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.contact_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.saveContact:

                EditText ime = findViewById(R.id.imeKontantk);
                EditText prezime = findViewById(R.id.prezimeKontakt);
                EditText email = findViewById(R.id.emailKontakt);

                String imeIzm = ime.getText().toString();
                String prezimeIzm = prezime.getText().toString();
                String emailIzm = email.getText().toString();

                Intent in = getIntent();
                Contact contact = (Contact) in.getExtras().getSerializable("CONTACT");

                if(imeIzm.trim().equals("")){
                    Toast.makeText(this,"Unesite ime",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(prezimeIzm.trim().equals("")){
                    Toast.makeText(this,"Unesite prezime",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(emailIzm.trim().equals("")){
                    Toast.makeText(this,"Unesite email",Toast.LENGTH_LONG).show();
                    return true;
                }

                ContactI me = RetrofitC.getClient().create(ContactI.class);

                Call<Contact> call = me.izmenaKontakta(contact.getId(),imeIzm,prezimeIzm,emailIzm);

                call.enqueue(new Callback<Contact>() {
                    @Override
                    public void onResponse(Call<Contact> call, Response<Contact> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Contact> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });

                Intent i23 = new Intent(ContactActivity.this,ContactsActivity.class);
                startActivity(i23);

                Toast.makeText(this, "Izmena Kont",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.brisanjeKontakt:
                Contact c = (Contact) getIntent().getSerializableExtra("CONTACT");
                ContactI service = RetrofitC.getClient().create(ContactI.class);
                Call<Void> call2 = service.brsianjeKontakta(c.getId());
                call2.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(ContactActivity.this,"Obrisan kontakt",Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(ContactActivity.this, ContactsActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(ContactActivity.this, "GRESKA", Toast.LENGTH_SHORT);
                    }
                });

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
