package com.example.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.androidapp.Retrofit.ContactI;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.model.Contact;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateContactActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Toolbar toolbar = findViewById(R.id.createContactToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create Contact");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.create_contact_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_create_contact:


                EditText imeE = findViewById(R.id.imeContact);
                EditText prezimeE = findViewById(R.id.prezimeContact);
                EditText emailE = findViewById(R.id.emailContact);

                ContactI me = RetrofitC.getClient().create(ContactI.class);

                String ime = imeE.getText().toString();
                String prezime = prezimeE.getText().toString();
                String email = emailE.getText().toString();

                if(ime.trim().equals("")){
                    Toast.makeText(this,"Unesite ime",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(prezime.trim().equals("")){
                    Toast.makeText(this,"Unesite prezime",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(email.trim().equals("")){
                    Toast.makeText(this,"Unesite email",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(email.trim().contains("@gmail.com")){
                    Toast.makeText(this,"Unesite ispravnu email adresu",Toast.LENGTH_LONG).show();
                    return true;
                }

                Call<Contact> call = me.dodavanjeKontakt(ime,prezime,email);

                call.enqueue(new Callback<Contact>() {
                    @Override
                    public void onResponse(Call<Contact> call, Response<Contact> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }



                    }

                    @Override
                    public void onFailure(Call<Contact> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                Intent i123 = new Intent(CreateContactActivity.this,ContactsActivity.class);
                startActivity(i123);

                Toast.makeText(this, "Dodali ste kontakt",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.close_create_contact:
                Intent i = new Intent(CreateContactActivity.this,ContactsActivity.class);
                startActivity(i);
                Toast.makeText(this, "Izlaz",Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
