package com.example.androidapp.Retrofit;

import com.example.androidapp.model.Contact;
import com.example.androidapp.model.EmailClass;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MessagesI {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET("messages")
    Call<ArrayList<EmailClass>> getMessages();

    @GET("sortAsc")
    Call<ArrayList<EmailClass>> getAllMessagesAsc();

    @GET("sortDesc")
    Call<ArrayList<EmailClass>> getAllMessagesDesc();

    @POST("messages/{to}/{tema}/{sadrzaj}")
    Call<EmailClass> dodacanjeEmail(@Path("to") String to, @Path("tema") String tema,
                                    @Path("sadrzaj") String sadrzaj);

    @PUT("messages/{id}")
    Call<EmailClass> izmenaMe(@Path("id") int id);

    @DELETE("messages/{id}")
    Call<Void> brisanjeEmail(@Path("id") int id);


}
