package com.example.androidapp.model;

import java.util.ArrayList;

public class Tag {

    private int id;
    private String name;
    private ArrayList<EmailClass> porukeT;


    public Tag(){
        super();
    }

    public Tag(int id, String name, ArrayList<EmailClass> porukeT) {
        this.id = id;
        this.name = name;
        this.porukeT = porukeT;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<EmailClass> getPorukeT() {
        return porukeT;
    }

    public void setPorukeT(ArrayList<EmailClass> porukeT) {
        this.porukeT = porukeT;
    }
}
