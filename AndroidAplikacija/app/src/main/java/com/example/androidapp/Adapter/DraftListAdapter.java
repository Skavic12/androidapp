package com.example.androidapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidapp.R;
import com.example.androidapp.model.EmailClass;

import java.util.ArrayList;

public class DraftListAdapter extends ArrayAdapter<EmailClass> {

    private Context context;
    private ArrayList<EmailClass> drafts;

    public DraftListAdapter(Context context, int resource, ArrayList<EmailClass> drafts) {
        super(context, resource, drafts);

        this.context = context;
        this.drafts = drafts;

    }

    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        EmailClass m = drafts.get(position);


        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_email_list, parent, false);

        }


        TextView txtKor = convertView.findViewById(R.id.korisnikEmailF);
        TextView txtTema = convertView.findViewById(R.id.as);
        TextView txtSadrzaj = convertView.findViewById(R.id.as1);

        if(m.getTo().getEmail() == null){
            txtKor.setText("Nedovrseno");
        }else{
            txtKor.setText(m.getTo().getEmail());
        }

        String tema = m.getTema();
        if(tema.length() > 20 ){
            tema = tema.substring(0,16) + "...";
        }
        if(tema.equals("a")) {
            tema = "Nedovrseno";
        }

        txtTema.setText(tema);

        String sadrzaj= m.getSadrzajPor();
        if(sadrzaj.length() > 35 ){
            sadrzaj = sadrzaj.substring(0,31) + "...";
        }

        if(sadrzaj.equals("a")) {
            sadrzaj = "Nedovrseno";
        }

        txtSadrzaj.setText(sadrzaj);


        return convertView;
    }

}
