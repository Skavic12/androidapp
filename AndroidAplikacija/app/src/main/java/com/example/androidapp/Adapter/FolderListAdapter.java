package com.example.androidapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidapp.FolderActivity;
import com.example.androidapp.R;
import com.example.androidapp.model.Folder;
import com.example.androidapp.model.Rule;

import java.util.ArrayList;

public class FolderListAdapter extends ArrayAdapter<Folder> {

    private Context context;
    int resource;
    private ArrayList<Folder> folders;

    public FolderListAdapter(Context context,int resourcel, ArrayList<Folder> folders) {
        super(context, R.layout.layout_list_folders,folders);

        this.context = context;
        this.resource = resourcel;
        this.folders = folders;

    }

    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        Folder f = folders.get(position);
        Rule r = getItem(position).getPravila();


        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);

        }


        TextView txtNaslov = convertView.findViewById(R.id.nazivFolderaL);
        TextView txtPravila = convertView.findViewById(R.id.ruleFolder);

        String name = getItem(position).getIme();


        txtNaslov.setText(name);
        txtPravila.setText(r.getCondition().toString() + "," + r.getOperation().toString());


        return convertView;
    }


}
