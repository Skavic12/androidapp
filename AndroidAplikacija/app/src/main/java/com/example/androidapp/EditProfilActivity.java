package com.example.androidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.androidapp.Retrofit.AccountService;
import com.example.androidapp.Retrofit.RetrofitC;
import com.example.androidapp.Retrofit.SaveSharedPreference;
import com.example.androidapp.model.Account;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfilActivity extends AppCompatActivity {
    private Account account = new Account();
    private Toolbar toolbar;
    private Toolbar supportActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);



    }
    @Override
    protected void onResume(){
        super.onResume();

        final EditText editTexProfileUsername  = findViewById(R.id.editTextProfileUsername);
        final EditText editTextProfilePassword = findViewById(R.id.editTextProfilePassword);
        final EditText editTextSmtpEdit = findViewById(R.id.smtpEdit);

        editTexProfileUsername.setText(account.getKorisnickoIme());
        editTextProfilePassword.setText(account.getSifra());
        editTextSmtpEdit.setText(account.getSmtp());



        Button btnizmenaEditProfile = findViewById(R.id.btnIzmeniEditProfile);
        btnizmenaEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTexProfileUsername.getText().toString().equals("") || editTextProfilePassword.getText().toString().equals("")) {
                    Toast.makeText(EditProfilActivity.this, "Niste uneli sve podatke", Toast.LENGTH_SHORT);
                }
                else {

                    account.setKorisnickoIme(editTexProfileUsername.getText().toString());
                    account.setSifra(editTextProfilePassword.getText().toString());
                    account.setSmtp(editTextSmtpEdit.getText().toString());
                    AccountService accountservice = RetrofitC.getClient().create(AccountService.class);
                    Call<Account> accountCall = accountservice.editAccount(account.getId(), account.getKorisnickoIme(), account.getSifra(), account.getPop3(), account.getSmtp());
                    accountCall.enqueue(new Callback<Account>() {
                        @Override
                        public void onResponse(Call<Account> call, Response<Account> response) {
                            account = response.body();

                            SaveSharedPreference.setLoggedIn(getApplicationContext(), account);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Account> call, Throwable t) {
                            Toast.makeText(EditProfilActivity.this,"Nesto nije uspelo", Toast.LENGTH_LONG).show();
                        }
                    });
                }


            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radiogroupedit:
                if (checked)
                    account.setPop3("pop3");
                break;
            case R.id.radio_imap:
                if (checked)
                    account.setImap("imap");
                break;
        }
    }
}
