package com.example.androidapp.Retrofit;

import com.example.androidapp.model.Folder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FoldersI {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET("folders")
    Call<ArrayList<Folder>> getFolders();

    @POST("folders/{ime}/{condition}/{operation}")
    Call<Folder> createFolder(
            @Path("ime") String name, @Path("condition") String condition, @Path("operation") String operation);

    @PUT("folders/{id}/{ime}/{condition}/{operation}")
    Call<Folder> updateFolders(@Path("id") int id, @Path("ime") String name, @Path("condition") String condition,
                               @Path("operation") String operation);


    @DELETE("folders/{id}")
    Call<Void> brisanjeFoldera(@Path("id") int id);

}