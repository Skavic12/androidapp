package model;

import java.util.ArrayList;

public class Contact {

    private int id;
    private String ime;
    private String prezime;
    private String email;
    private Photo display;
    private enum format {PLAIN, HTML};
    private ArrayList<EmailClass> from;
    private ArrayList<EmailClass> to;
    private ArrayList<EmailClass> cc;
    private ArrayList<EmailClass> bcc;



    public Contact() {
        super();
    }

    public Contact(int id, String ime, String prezime, String email, Photo display, ArrayList<EmailClass> from, ArrayList<EmailClass> to, ArrayList<EmailClass> cc, ArrayList<EmailClass> bcc) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.display = display;
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Photo getDisplay() {
        return display;
    }

    public void setDisplay(Photo display) {
        this.display = display;
    }

    public ArrayList<EmailClass> getFrom() {
        return from;
    }

    public void setFrom(ArrayList<EmailClass> from) {
        this.from = from;
    }

    public ArrayList<EmailClass> getTo() {
        return to;
    }

    public void setTo(ArrayList<EmailClass> to) {
        this.to = to;
    }

    public ArrayList<EmailClass> getCc() {
        return cc;
    }

    public void setCc(ArrayList<EmailClass> cc) {
        this.cc = cc;
    }

    public ArrayList<EmailClass> getBcc() {
        return bcc;
    }

    public void setBcc(ArrayList<EmailClass> bcc) {
        this.bcc = bcc;
    }
    
    @Override
    public boolean equals(Object obj) {
    	Contact contact = (Contact) obj;
    	return this.email.equals(contact.email);
    }
}
