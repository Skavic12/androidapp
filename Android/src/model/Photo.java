package model;

public class Photo {

    private int id;
    private String putanja;
    private Contact contact;

    public  Photo(){
        super();
    }

    public Photo(int id, String putanja, Contact contact) {
        this.id = id;
        this.putanja = putanja;
        this.contact = contact;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPutanja() {
        return putanja;
    }

    public void setPutanja(String putanja) {
        this.putanja = putanja;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
