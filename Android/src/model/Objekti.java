package model;

import java.util.ArrayList;
import java.util.Date;

public class Objekti {

        private Folder folder;
        private EmailClass message;
        private Contact contact;
        private ArrayList<Contact> contacts = new ArrayList<>();
        private ArrayList<Folder> folders = new ArrayList<>();
        private ArrayList<EmailClass> emails = new ArrayList<>();

        public Objekti() {

            folder = new Folder();
            folder.setId(1);
            folder.setIme("Folder");
            folder.setPorukeFoldera(new ArrayList<EmailClass>());
            folders.add(folder);

            folder = new Folder();
            folder.setId(2);
            folder.setIme("Mungosi");
            folder.setPorukeFoldera(new ArrayList<EmailClass>());
            folders.add(folder);

            folder = new Folder();
            folder.setId(3);
            folder.setIme("Poslovno");
            folder.setPorukeFoldera(new ArrayList<EmailClass>());
            folders.add(folder);

            folder = new Folder();
            folder.setId(4);
            folder.setIme("Zezanje");
            folder.setPorukeFoldera(new ArrayList<EmailClass>());
            folders.add(folder);

            Contact contact2 = new Contact();
            contact2.setId(3);
            contact2.setIme("Ilija");
            contact2.setPrezime("Ilkic");
            contact2.setEmail("asdasdasdsa@gmail.com");
            contacts.add(contact);

            message = new EmailClass();
            message.setId(1);
            message.setFrom(contact2);
            message.setVreme(new Date(2019-4-23));
            message.setTema("Hello");
            message.setSadrzajPor("Message1");
            folder.getPorukeFoldera().add(message);
            emails.add(message);


            Contact contact = new Contact();
            contact.setId(2);
            contact.setIme("Nenad");
            contact.setPrezime("Skavic");
            contact.setEmail("SFSFSD@gmail.com");
            contacts.add(contact);

            message = new EmailClass();
            message.setId(2);
            message.setFrom(contact);
            message.setVreme(new Date(2011-3-22));
            message.setTema("sdasd");
            message.setSadrzajPor("Message12");
            folder.getPorukeFoldera().add(message);
            emails.add(message);

            Contact contact1 = new Contact();
            contact1.setId(1);
            contact1.setIme("Zeljko111");
            contact1.setPrezime("Kostic222");
            contact1.setEmail("asdasdasasda@gmail.com");
            contacts.add(contact);

            message = new EmailClass();
            message.setId(3);
            message.setFrom(contact1);
            message.setVreme(new Date(2019-2-21));
            message.setTema("adasdasasdasdasdsadsa");
            message.setSadrzajPor("12131321saddasdsaddsadssaddassadadsdaaddassadsadassdasad");
            folder.getPorukeFoldera().add(message);
            emails.add(message);
        }

        public Folder getFolder() {
            return folder;
        }

        public void setFolder(Folder folder) {
            this.folder = folder;
        }

        public EmailClass getMessage() {
            return message;
        }

        public void setMessage(EmailClass message) {
            this.message = message;
        }

        public Contact getContact() {
            return contact;
        }

        public void setContact(Contact contact) {
            this.contact = contact;
        }

        public ArrayList<Contact> getContacts() {
            return contacts;
        }

        public void setContacts(ArrayList<Contact> contacts) {
            this.contacts = contacts;
        }

        public ArrayList<Folder> getFolders() {
            return folders;
        }

        public void setFolders(ArrayList<Folder> folders) {
            this.folders = folders;
        }

    public ArrayList<EmailClass> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<EmailClass> emails) {
        this.emails = emails;
    }
}
