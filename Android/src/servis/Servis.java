package servis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Account;
import model.Attachment;
import model.Contact;
import model.EmailClass;
import model.Folder;
import model.Rule;
import model.Rule.Condition;
import model.Rule.Operation;


@Path("/service")
public class Servis {
	   public static ArrayList<Contact> contacts = new ArrayList<>();
	    private static ArrayList<Folder> folders = new ArrayList<>();
	    public static ArrayList<EmailClass> emails = new ArrayList<>();
		private static ArrayList<Account> accounts = new ArrayList<>();
		private static ArrayList<EmailClass> drafts = new ArrayList<>();
//		private static CheckEmail emailChecker = new CheckEmail();
		
		static {
	       
			//initEmailChecker();

	        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Date date=new Date();
			Date date1=new Date();
			Date date2=new Date();
			String dat="2019/06/24 14:15";
			String dat1="2019/05/22 16:15";
			String dat2="2019/02/27 18:15";
			try {
				 date = dateFormat.parse(dat);
				 date1 = dateFormat.parse(dat1);
				 date2 = dateFormat.parse(dat2);
			} catch (ParseException e) {
				
				e.printStackTrace();
			}
			
	        
	        Account account2 = new Account();
	        account2.setKorisnickoIme("b");
	        account2.setId(2);
	        account2.setSifra("b");
	        account2.setSmtp("smtp");
	        account2.setPop3("pop3");
	        account2.setSvePoruke(new ArrayList<EmailClass>());
	        
	       

	        Rule r2 = new Rule();
	        r2.setId(2);
	        r2.setCondition(Condition.FROM);
	        r2.setOperation(Operation.MOVE);

	        

	        Folder folder2 = new Folder();
	        folder2.setId(2);
	        folder2.setIme("Primljene");
	        folder2.setFolderi(new ArrayList<Folder>());
	        folder2.setPravila(r2);
	        folder2.setPorukeFoldera(emails);
	        folders.add(folder2);
	        

	        Contact contact2 = new Contact();
	        contact2.setId(3);
	        contact2.setIme("Ilija");
	        contact2.setPrezime("Ilkic");
	        contact2.setEmail("test.ilkic@gmail.com");
	  
	        contacts.add(contact2);

	        EmailClass message1 = new EmailClass();
	        message1.setId(1);
	        message1.setFrom(contact2);
	        message1.setTo(contact2);
	        message1.setVreme(date1);
	        message1.setTema("Hello");
	        message1.setPrilog(new ArrayList<Attachment>());
	        message1.setSadrzajPor("Message1");
	        message1.setSeen(true);
	        emails.add(message1);


	        Contact contact1 = new Contact();
	        contact1.setId(2);
	        contact1.setIme("Nenad");
	        contact1.setPrezime("Skavic");
	        contact1.setEmail("SF412017@gmail.com");
	        
	        contacts.add(contact1);

	        EmailClass message2 = new EmailClass();
	        message2.setId(2);
	        message2.setFrom(contact1);
	        message2.setTo(contact2);
	        message2.setVreme(date);
	        message2.setTema("Nenad");
	        message2.setSeen(true);
	        message2.setPrilog(new ArrayList<Attachment>());
	        message2.setSadrzajPor("Message12");
	        emails.add(message2);

	        Contact contact4 = new Contact();
	        contact4.setId(1);
	        contact4.setIme("Zeljko111");
	        contact4.setPrezime("Kostic222");
	        contact4.setEmail("asdasdasasda@gmail.com");
	        
	        contacts.add(contact4);
	        
	        ArrayList<Attachment> atach = new ArrayList<Attachment>();
	        
	        Attachment a1 = new Attachment();
	        a1.setId(1);
	        a1.setIme("video");
	        
	        Attachment a2 = new Attachment();
	        a2.setId(1);
	        a2.setIme("pesma");

	        atach.add(a1);
	        atach.add(a2);
	        
	        EmailClass message3 = new EmailClass();
	        message3.setId(3);
	        message3.setFrom(contact4);
	        message3.setTo(contact2);
	        message3.setVreme(date1);
	        message3.setTema("adasdasasdasdasdsadsa");
	        message3.setSadrzajPor("12131321saddasdsaddsadssaddassadadsdaaddassadsadassdasad");
	        message3.setSeen(true);
	        message3.setPrilog(atach);
	        account2.getSvePoruke().add(message3);
	        
	        emails.add(message3);
	        
	        accounts.add(account2);
	        

	       
		}

	//
//		private static void initEmailChecker() {
//			CheckEmailTask task = new CheckEmailTask();
//			Timer timer = new Timer(true);
//	        timer.scheduleAtFixedRate(task, 0, 30*1000);
//		}
	//	
		
		
		@GET
		@Path("/messages")
		public Response get(){
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			
			String json;
			
			try {
				json = mapper.writeValueAsString(emails);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
			
			//emailChecker.checkMail();
			
			return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
		}
		
		@POST
		@Path("/messages/{to}/{tema}/{sadrzaj}")
		public void addNewEmail(@PathParam("to") String to,
				@PathParam("tema") String tema,
				@PathParam("sadrzaj") String sadrzaj) {

			Date dateAdd=new Date();
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
			df.setTimeZone(tz);
			df.format(dateAdd);
			
			Contact contact4 = new Contact();
	        contact4.setId(1);
	        contact4.setIme("Zeljko111");
	        contact4.setPrezime("Kostic222");
	        contact4.setEmail("asdasdasasda@gmail.com");
	        
			
			int provera = 0;
			EmailClass e = new EmailClass();
			
			for(Contact c : contacts) {
				if(c.getEmail().equals(to)) {
					provera = 1;
					System.out.println("Ima korisnik");
					e.setId(hashCode());
					e.setFrom(contact4);
					e.setTo(c);
					e.setTema(tema);
					e.setSadrzajPor(sadrzaj);
					e.setBcc(new ArrayList<Contact>());
					e.setCc(new ArrayList<Contact>());
					e.setVreme(dateAdd);
					e.setSeen(false);
					e.setPrilog(new ArrayList<Attachment>());
					System.out.println(e);
					emails.add(e);
				}
				
			}if(provera == 0) {
				Contact newCon = new Contact();
				newCon.setEmail(to);
				System.out.println("NEMA korisnik");
				e.setId(hashCode());
				e.setFrom(contact4);
				e.setTo(newCon);
				e.setTema(tema);
				e.setSadrzajPor(sadrzaj);
				e.setBcc(new ArrayList<Contact>());
				e.setCc(new ArrayList<Contact>());
				e.setVreme(dateAdd);
				e.setSeen(false);
				e.setPrilog(new ArrayList<Attachment>());
				System.out.println(e);
				emails.add(e);
			}	
		}
		
		@PUT
		@Path("/messages/{id}")
		public Response editMesage(@PathParam("id") int id) {
			ObjectMapper mapper = new ObjectMapper();
			String json;
			for (EmailClass e : emails) {
				if (e.getId() == id) {
					e.setSeen(true);
					try {
						json = mapper.writeValueAsString(e);
						System.out.println("vidjen");
						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					} catch (JsonProcessingException ez) {
						ez.printStackTrace();
						return Response.status(500).build();
					}
				}
			}
			return Response.status(404).build();
		}
		
		
		@GET 
		@Path("/drafts")
		public Response getDrafts() {
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			
			String json;
			
			try {
				json = mapper.writeValueAsString(drafts);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
			
			return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
		}

		
		@POST
		@Path("/draft/{toD}/{temaD}/{sadrzajD}")
		public void addDraft(@PathParam("toD") String to,
				@PathParam("temaD") String tema,
				@PathParam("sadrzajD") String sadrzaj,@PathParam("as") String as) {
			
			Contact contact4 = new Contact();
	        contact4.setId(1);
	        contact4.setIme("Zeljko111");
	        contact4.setPrezime("Kostic222");
	        contact4.setEmail("asdasdasasda@gmail.com");
	        
	        Date dateAdd=new Date();
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
			df.setTimeZone(tz);
			df.format(dateAdd);
			
			int provera = 0;
			EmailClass e = new EmailClass();
			
			for(Contact c : contacts) {
				if(c.getEmail().equals(to)) {
					provera = 1;
					System.out.println("Ima korisnik");
					e.setId(hashCode());
					e.setFrom(contact4);
					e.setTo(c);
					e.setTema(tema);
					e.setSadrzajPor(sadrzaj);
					e.setBcc(new ArrayList<Contact>());
					e.setCc(new ArrayList<Contact>());
					e.setPrilog(new ArrayList<Attachment>());
					e.setVreme(dateAdd);
					System.out.println(e);
					drafts.add(e);
					}
				
			}if(provera == 0) {
				Contact newCon = new Contact();
				newCon.setEmail(to);
				System.out.println("NEMA korisnik");
				e.setId(hashCode());
				e.setFrom(contact4);
				e.setTo(newCon);
				e.setTema(tema);
				e.setSadrzajPor(sadrzaj);
				e.setBcc(new ArrayList<Contact>());
				e.setCc(new ArrayList<Contact>());
				e.setVreme(dateAdd);
				e.setPrilog(new ArrayList<Attachment>());
				System.out.println(e);
				drafts.add(e);
				}
			
			
			}
		
		@PUT
		@Path("/drafts/{id}/{to}/{tema}/{sadrzaj}")
		public Response editDraft(@PathParam("id") int id,@PathParam("to") String to,
				@PathParam("tema") String tema,
				@PathParam("sadrzaj") String sadrzaj) {
			ObjectMapper mapper = new ObjectMapper();
			String json;
			int provera = 0;
			for (EmailClass ec : drafts) {
				if (ec.getId() == id) {
					for(Contact c : contacts) {
						if(c.getEmail().equals(to)) {
							provera = 1;
							System.out.println("Ima korisnik");
							ec.setTo(c);
							ec.setTema(tema);
							ec.setSadrzajPor(sadrzaj);
							emails.add(ec);
							drafts.remove(ec);
						}
						
					}if(provera == 0) {
						Contact newCon = new Contact();
						newCon.setEmail(to);
						System.out.println("NEMA korisnik");
						ec.setTo(newCon);
						ec.setTema(tema);
						ec.setSadrzajPor(sadrzaj);
						emails.add(ec);
						drafts.remove(ec);
					}
					
					try {
						json = mapper.writeValueAsString(ec);
						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					} catch (JsonProcessingException e) {
						e.printStackTrace();
						return Response.status(500).build();
					}
				}
			}
			return Response.status(404).build();
		}
		
		@DELETE
		@Path("/drafts/{id}")
		public void deleteDraftMessages(@PathParam("id") int id){
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			EmailClass m=new EmailClass();
			for (EmailClass e : drafts) {
				if(e.getId()==id)
					m=e;
			}
			if(m != null) {
				drafts.remove(m);
							
			}
		}
					
		@DELETE
		@Path("/messages/{id}")
		public void deleteMessages(@PathParam("id") int id){
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			EmailClass m=new EmailClass();
			for (EmailClass e : emails) {
				if(e.getId()==id)
					m=e;
			}
			if(m != null) {
				emails.remove(m);
							
			}
		}
		
		@GET
		@Path("/sortAsc")
		public Response getSortAsc() {
			emails.sort(Comparator.comparing(EmailClass :: getVreme));
			ObjectMapper mapper = new ObjectMapper();
			String json;
			try {
				json = mapper.writeValueAsString(emails);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
			
			return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
		}
		
		@GET
		@Path("/sortDesc")
		public Response getSortDesc() {
			emails.sort(Comparator.comparing(EmailClass :: getVreme).reversed());
			ObjectMapper mapper = new ObjectMapper();
			String json;
			try {
				json = mapper.writeValueAsString(emails);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
			
			return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
		}
		
		
		
		@GET 
		@Path("/folders")
		public Response getFolderData() {
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			
			String json;
			
			try {
				json = mapper.writeValueAsString(folders);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
			
			return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
		}
		

		
		@POST
		@Path("/folders/{ime}/{condition}/{operation}")
		public void addNewFolder(@PathParam("ime") String name,
				@PathParam("condition") String condition,
				@PathParam("operation") String operation) {
			
			Folder folder = new Folder();
			folder.setId(hashCode());
			folder.setIme(name);
			folder.setPravila(new Rule(1,Condition.valueOf(condition),Operation.valueOf(operation)));
			folder.setPorukeFoldera(new ArrayList<EmailClass>());
			folder.setFolderi(new ArrayList<Folder>());
			System.out.println(folder);
			folders.add(folder);
			
		}
		
		
		@PUT
		@Path("/folders/{id}/{ime}/{condition}/{operation}")
		public Response editFolder(@PathParam("id") int id, @PathParam("ime") String name,
				@PathParam("condition") String condition,
				@PathParam("operation") String operation) {
			ObjectMapper mapper = new ObjectMapper();
			String json;
			for (Folder folder : folders) {
				if (folder.getId() == id) {
					folder.setIme(name);
					folder.getPravila().setCondition(Condition.valueOf(condition));
					folder.getPravila().setOperation(Operation.valueOf(operation));
					try {
						json = mapper.writeValueAsString(folder);
						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					} catch (JsonProcessingException e) {
						e.printStackTrace();
						return Response.status(500).build();
					}
				}
			}
			return Response.status(404).build();
		}
		
		
		@DELETE
		@Path("/folders/{id}")
		public void deleteFolder(@PathParam("id") int id){
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			Folder cc=new Folder();
			for (Folder c : folders) {
				if(c.getId()==id)
					cc=c;
			}
			if(cc != null)
				folders.remove(cc);
		}
		
		@GET
		@Path("/contacts")
		public Response getContacts(){
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			
			String json;
			
			try {
				json = mapper.writeValueAsString(contacts);
				
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(500).build();
			}
			
			return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();

		}
		
		@POST
		@Path("/contacts/{ime}/{prezime}/{email}")
		public Response createFolder(@PathParam("ime") String ime,@PathParam("prezime") String prezime,@PathParam("email") String email) {
			
			String json;
			ObjectMapper mapper = new ObjectMapper();
			
			try {
				Contact co= new Contact();
				co.setId(hashCode());
				co.setIme(ime);
				co.setPrezime(prezime);
				co.setEmail(email);
		//		Photo p = new Photo();
		//		p.setId(hashCode());
		//		p.setPutanja("https://www.w3schools.com/howto/img_avatar2.png");
		//		co.setDisplay(p);
				
				
				
				contacts.add(co);
				
				json = mapper.writeValueAsString(co);
				
				return Response.status(200).build();
			}catch (JsonProcessingException e) {
				e.printStackTrace();
				return Response.status(500).build();
			}
			
				
		}
		
		@PUT
		@Path("contacts/{id}/{ime}/{prezime}/{email}")
		public Response UpdateContact(@PathParam("id") int id, @PathParam("ime") String ime,
				@PathParam("prezime") String prezime, @PathParam("email") String email) {	
			System.out.println("PROSAO ACCOUNT!");
			String json;

			ObjectMapper mapper = new ObjectMapper();
			for (Contact con : contacts) {
				if (con.getId() == id) {
					try {
						
						con.setIme(ime);
						con.setPrezime(prezime);
						con.setEmail(email);
						
						
						
						
						System.out.println(con);
						json = mapper.writeValueAsString(con);

						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					} catch (JsonProcessingException e) {
						e.printStackTrace();
						return Response.status(500).build();
					}
				}
			}

			return Response.status(404).build();
		
		}
		
		@DELETE
		@Path("/contacts/{id}")
		public void deleteAccount(@PathParam("id") int id){
			System.out.println(LocalDateTime.now().toString() + "Sada");
			ObjectMapper mapper = new ObjectMapper();
			Contact cc=new Contact();
			for (Contact c : contacts) {
				if(c.getId()==id)
					cc=c;
			}
			if(cc != null)
				contacts.remove(cc);
		}

		
		@GET
		@Path("/login/{username}/{password}")
		public Response login(@PathParam("username")String username,@PathParam("password") String password) {
			String json;
			ObjectMapper mapper = new ObjectMapper();
			for(Account acc : accounts ) {
				if(acc.getKorisnickoIme().equals(username)&&acc.getSifra().equals(password)) {
					try {
						json = mapper.writeValueAsString(acc);
						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					}catch (JsonProcessingException e) {
						e.printStackTrace();
						return Response.status(500).build();
					}
				}
				
			}return Response.status(404).build();
		}
		
		@GET
		@Path("/accounts/{username}")
		public Response getAcount(@PathParam("username")String username) {
			String json;
			ObjectMapper mapper = new ObjectMapper();
			for(Account acc : accounts ) {
				if(acc.getKorisnickoIme().equals(username)) {
					try {
						json = mapper.writeValueAsString(acc);
						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					}catch (JsonProcessingException e) {
						e.printStackTrace();
						return Response.status(500).build();
					}
				}
				
			}return Response.status(404).build();
		}
		

		@PUT
		@Path("/account/{id}/{username}/{password}/{smtp}")
		public Response editAccount(@PathParam("id") int id, @PathParam("username") String username,
				@PathParam("password") String password,
				@PathParam("smtp") String smtp) {
			System.out.println("PROSAO ACCOUNT!");
			String json;

			ObjectMapper mapper = new ObjectMapper();
			for (Account findAccount : accounts) {
				if (findAccount.getId() == id) {
					try {
						findAccount.setKorisnickoIme(username);
						findAccount.setSifra(password);
						findAccount.setSmtp(smtp);
					//	findAccount.setPop3(pop3Imap);
						System.out.println(findAccount);
						json = mapper.writeValueAsString(findAccount);

						return Response.status(200).entity(json).type(MediaType.APPLICATION_JSON).build();
					} catch (JsonProcessingException e) {
						e.printStackTrace();
						return Response.status(500).build();
					}
				}
			}

			return Response.status(404).build();
		
			
		}

		
}
