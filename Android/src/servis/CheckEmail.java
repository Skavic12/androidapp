package servis;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import model.Contact;
import model.EmailClass;

public class CheckEmail {
	
   public static void checkMail() 
   {
      try {
      //Set property values
      Properties propvals = new Properties();
      propvals.put("mail.store.protocol", "imaps");
      propvals.put("mail.imaps.port", "993");
      propvals.put("mail.imaps.starttls.enable", "true");
      Session emailSessionObj = Session.getDefaultInstance(propvals);  
      //Create IMAPS store object and connect with the server
      Store storeObj = emailSessionObj.getStore("imaps");
      storeObj.connect("imap.gmail.com", "test.ilkic@gmail.com", "ilijatest");
      //Create folder object and open it in read-only mode
      Folder emailFolderObj = storeObj.getFolder("INBOX");
      emailFolderObj.open(Folder.READ_ONLY);
      Flags seen = new Flags(Flags.Flag.SEEN);
      FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
      Message[] messageobjs = emailFolderObj.search(unseenFlagTerm);
 
      for (int i = 0, n = messageobjs.length; i < n; i++) {
         Message indvidualmsg = messageobjs[i];
         System.out.println("Printing individual messages");
         System.out.println("No# " + (i + 1));
         System.out.println("Email Subject: " + indvidualmsg.getSubject());
         System.out.println("Sender: " + indvidualmsg.getFrom()[0]);
         System.out.println("Content: " + indvidualmsg.getContent().toString());
         
         EmailClass message = new EmailClass();
         message.setId(1);
         message.setFrom(findContact(((InternetAddress)indvidualmsg.getFrom()[0]).getAddress()));
         message.setTo(findContact(((InternetAddress)indvidualmsg.getRecipients(Message.RecipientType.TO)[0]).getAddress()));
         message.setVreme(indvidualmsg.getSentDate());
         message.setTema(indvidualmsg.getSubject());
         message.setSadrzajPor(getTextFromMimeMultipart(((MimeMultipart)indvidualmsg.getContent())));
         
         if(!Servis.emails.contains(message)) {
        	 Servis.emails.add(message);
         }
      }
      //Now close all the objects
      emailFolderObj.close(false);
      storeObj.close();
      } catch (NoSuchProviderException exp) {
         exp.printStackTrace();
      } catch (MessagingException exp) {
         exp.printStackTrace();
      } catch (Exception exp) {
         exp.printStackTrace();
      }
   }
   
   private static Contact findContact(String contactEmail) {
	   for(Contact contact: Servis.contacts) {
		   if(contact.getEmail().equals(contactEmail)) {
			   return contact;
		   }
	   }
	   return null;
   }
   
   private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/plain")) {
	            result = result + "\n" + bodyPart.getContent();
	            break; // without break same text appears twice in my tests
	        } else if (bodyPart.isMimeType("text/html")) {
	            result = result + "\n" + bodyPart.getContent();
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    return result;
	}
}